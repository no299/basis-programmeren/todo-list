package nl.novi.basisprogrammeren;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListFileUtils {

    private final CryptoUtils cryptoUtils;
    private static final String FILE_PATH = "list.txt";

    public ListFileUtils(CryptoUtils cryptoUtils) {
        this.cryptoUtils = cryptoUtils;
    }

    public List<TodoItem> readFile() {
        List<TodoItem> items = new ArrayList<>();
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(FILE_PATH));
            String content = cryptoUtils.decrypt(bytes);
            List<String> lines = Arrays.stream(content.split("\\n")).toList();
            for (String line : lines) {
                String[] values = line.split("\\|");
                items.add(new TodoItem(values[0], values[1].equals("true")));
            }
        } catch (IOException e) {
            return new ArrayList<>();
        }
        return items;
    }

    public void writeFile(List<TodoItem> items) {
        String content = "";
        for (TodoItem item : items) {
            content = content + item.toString() + "\n";
        }
        try {
            byte[] bytes = cryptoUtils.encrypt(content);
            Files.write(Path.of(FILE_PATH), bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
