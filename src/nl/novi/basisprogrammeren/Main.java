package nl.novi.basisprogrammeren;

public class Main {

    public static void main(String[] args) {
        CryptoUtils cryptoUtils = new CryptoUtils(args[0]);
        ListFileUtils listFileUtils = new ListFileUtils(cryptoUtils);
        TodoListService service = new TodoListService(listFileUtils);
        TodoProgram program = new TodoProgram(service);
        program.run();
    }
}
