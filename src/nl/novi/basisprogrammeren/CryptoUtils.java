package nl.novi.basisprogrammeren;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class CryptoUtils {

    private final Key secretKey;

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";

    public CryptoUtils(String password) {
        byte[] passwordBytes = Arrays.copyOf(password.getBytes(), 16);
        secretKey = new SecretKeySpec(passwordBytes, ALGORITHM);
    }

    public byte[] encrypt(String content) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            return cipher.doFinal(content.getBytes());
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException ex) {
            throw new RuntimeException("Encryption error", ex);
        }
    }

    public String decrypt(byte[] inputBytes) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            byte[] outputBytes = cipher.doFinal(inputBytes);
            return new String(outputBytes);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException ex) {
            throw new RuntimeException("Encryption error", ex);
        }
    }
}
