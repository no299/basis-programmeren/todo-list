package nl.novi.basisprogrammeren;

public class TodoItem {

    private String title;
    private boolean done;

    public TodoItem(String title, boolean done) {
        this.title = title;
        this.done = done;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDone() {
        return done;
    }

    @Override
    public String toString() {
        return title + "|" + done;
    }
}
