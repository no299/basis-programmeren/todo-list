package nl.novi.basisprogrammeren;

import java.util.Scanner;

public class TodoProgram {

    private TodoListService service;
    private boolean running = true;
    private Scanner input = new Scanner(System.in);

    public TodoProgram(TodoListService service) {
        this.service = service;
    }

    public void run() {
        while (running) {
            service.print();
            System.out.println("To quit press \"q\" | To add an item press \"a\" | To remove an item press \"r\" | To edit an item press \"e\"");
            String option = input.nextLine();
            executeOption(option);
        }
    }

    private void executeOption(String option) {
        switch (option) {
            case "q":
                quit();
                break;
            case "a":
                addItem();
                break;
            case "r":
                removeItem();
                break;
            case "e":
                editItem();
                break;
            default:
                System.out.println("Invalid input");
        }
    }

    private void quit() {
        running = false;
        service.save();
    }

    private void addItem(){
        System.out.println("Please input the title of the new item.");
        String title = input.nextLine();
        TodoItem item = new TodoItem(title, false);
        service.add(item);
    }

    private void removeItem() {
        System.out.println("Please input index of the item to remove.");
        String stringIndex = input.nextLine();
        int index = Integer.parseInt(stringIndex);
        service.remove(index);
    }

    private void editItem() {
        System.out.println("Please input index of the item to edit.");
        String stringIndex = input.nextLine();
        int index = Integer.parseInt(stringIndex);

        System.out.println("Please input the new title.");
        String title = input.nextLine();
        String isDone = input.nextLine();
        TodoItem item = new TodoItem(title, isDone.equals("true"));

        service.edit(index, item);
    }
}
