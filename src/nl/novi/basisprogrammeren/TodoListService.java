package nl.novi.basisprogrammeren;

import java.util.List;

public class TodoListService {

    private final ListFileUtils listFileUtils;
    private List<TodoItem> items;

    public TodoListService(ListFileUtils listFileUtils) {
        this.listFileUtils = listFileUtils;
        this.items = listFileUtils.readFile();
    }

    public void add(TodoItem item) {
        items.add(item);
    }

    public void remove(int index) {
        items.remove(index);
    }

    public void print() {
        System.out.println("Items to do");
        System.out.println("------------");
        for (int index = 0; index < items.size(); index++) {
            TodoItem item = items.get(index);
            System.out.println(index + ": " + item.getTitle() + " - " + item.isDone());
        }
        System.out.println("------------");
    }

    public void edit(int index, TodoItem item) {
        items.add(index + 1, item);
        items.remove(index);
    }

    public void save() {
        listFileUtils.writeFile(items);
    }
}
